# Analysis of airplane crashes
My task was to visualize the following:
* Yearly how many airplanes crashed?
* How many people were on board?
* How many survived?

[Dataset - Kaggle](https://www.kaggle.com/saurograndi/airplane-crashes-since-1908)  
[Source code - GitLab](https://gitlab.com/SJort/airplane-crashes)  
[Shiny publication](https://jort.shinyapps.io/Airplane-Crashes/)  
